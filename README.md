# README #

### What is this repository for? ###

* Any job application!
* Directory structure and Makefiles can be used to adapt all source files

### How do I get set up? ###

List of minimum requirements:

* GNU make, wget & perl-tk packages
* a minimal installation of *TexLive* (CI runs on latest release)
* pdfunite
    * on *Linux* the package should be called `poppler-utils`
    * on *macos* use *Homebrew*,
        * install Homebrew with `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 2> /dev/null`
        * then install the package with `brew install poppler`

### How do I use it? ###

* editing the `example_required_tex_packages.txt` file with the packages required to generate your set of documents and install them (or check if they are already installed) with `` tlmgr install `cat example_required_tex_packages.txt` ``

Each document can be edited separately and has its Makefile.
A global Makefile allows to operate on all documents at the same time.

* run `make`
    * from the root directory to generate all documents in PDF, and unify them in one if needed
    * from each subdirectory to generate just that PDF document
* run `make unify` from the root directory to merge single PDFs documents in one
* run `make clean` as for `make` to remove build files