all: cover_letter/build/cover_letter.pdf CV/build/CV.pdf statement/build/statement.pdf unify

cover_letter/build/cover_letter.pdf:
	make -C cover_letter

CV/build/CV.pdf:
	make -C CV

statement/build/statement.pdf:
	make -C statement

unify:
	pdfunite cover_letter/build/cover_letter.pdf CV/build/CV.pdf statement/build/statement.pdf my-job-application.pdf

clean:
	make -C cover_letter clean
	make -C CV clean
	make -C statement clean
